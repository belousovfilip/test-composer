<?php

namespace Belfil\Fan;

use Faker\Factory;
use Faker\Generator;

class MyFan
{
    public static function instance(): Generator
    {
        return Factory::create();
    }

}